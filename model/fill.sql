
-- Use script: sudo mysql < fill.sql;

-- DATABASE:    polart
-- TYPE ENUM:   
-- TABLE:       
--              
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use polart;
-- show tables;
-- describe table...;
-- select * from table...;
-- quit;
-- exit;
-- -------------------------------------------------------------

USE polart;

-- TABLE Produit  -----------------------------

INSERT INTO evenement (nom, date_evt, img_princ)
    VALUES 
            -- 17 Productions
            ( "Production 01", NULL, "prod01ajardin.jpeg"),
            ( "Production 02", NULL, "prod02acuis.jpeg"),
            ( "Production 03", "2017-06-01", "prod03courcopie1.jpeg"),
            ( "Production 04", NULL, "prod03courcopie2.jpeg"),
            ( "Production 05", NULL, "prod03courcopie3.jpeg"),
            ( "Production 06", NULL, "prod03courcopie4.jpeg"),
            ( "Production 07", NULL, "prod03courcopie5.jpeg"),
            ( "Production 08", "2019-06-01", "prod03courcopie6.jpeg"),
            ( "Production 09", NULL, 4, "prod03courcopie7.jpeg"),
            ( "Production 10", NULL, 4, "prod03courcopie8.jpeg"),
            ( "Production 11", NULL, 4, "prod03courcopie9.jpeg"),
            ( "Production 12", NULL, 4, "prod03courcopie10.jpeg"),
            ( "Production 13", NULL, 4, "prod03courcopie11.jpeg"),
            ( "Production 14", NULL, 4, "prod03courcopie12.jpeg"),
            ( "Production 15", NULL, 4, "prod03courcopie13.jpeg"),
            ( "Production 16", NULL, 4, "prod03courcopie14.jpeg"),
            ( "Production 17", NULL, 4, "prod03courcopie15.jpeg"),

            ("Cuisine 01", NULL, 3, "cuisine1.jpeg"),
            ("Cuisine 02", NULL, 3, "cuisine2.jpeg"),
            ("Cuisine 03", NULL, 3, "cuisine3.jpeg"),
            ("Cuisine 04", "2019-06-02", 3, "cuisine4.jpeg"),
            ("Cuisine 05", NULL, 3, "cuisine5.jpeg"),
            ("Cuisine 06", NULL, 3, "cuisine6.jpeg"),
            ("Cuisine 07", NULL, 3, "cuisine7.jpeg"),

            ("Cour 01", NULL, 2, "cours1.jpeg"),
            ("Cour 02", NULL, 2, "cours2.jpeg"),
            ("Cour 03", "2019-11-22", 2, "cours3.jpeg"),
            ("Cour 04", NULL, 2, "cours4.jpeg"),
            ("Cour 05", NULL, 2, "cours5.jpeg"),
            
            ("Jardin 01", "2019-09-09", 1, "jardin1.jpeg"),
            ("Jardin 02", NULL, 1, "jardin2.jpeg"),
            ("Jardin 03", NULL, 1, "jardin3.jpeg"),
            ("Jardin 04", NULL, 1, "jardin4.jpeg");


INSERT INTO images(fichier)
    VALUES 
            ( "prod01ajardin.jpeg"),
            ( "prod01bjardin.jpeg"),
            ( "prod01cjardin.jpeg"),

            ( "prod02acuis.jpeg"),
            ( "prod02bcuis.jpeg"),
            ( "prod02ccuis.jpeg"),

            ( "prod03courcopie1.jpeg"),
            ( "prod03courcopie2.jpeg"),
            ( "prod03courcopie3.jpeg"),
            ( "prod03courcopie4.jpeg"),
            ( "prod03courcopie5.jpeg"),
            ( "prod03courcopie6.jpeg"),
            ( "prod03courcopie7.jpeg"),
            ( "prod03courcopie8.jpeg"),
            ( "prod03courcopie9.jpeg"),
            ( "prod03courcopie10.jpeg"),
            ( "prod03courcopie11.jpeg"),
            ( "prod03courcopie12.jpeg"),
            ( "prod03courcopie13.jpeg"),
            ( "prod03courcopie14.jpeg"),
            ( "prod03courcopie15.jpeg"),

            ( "cuisine1.jpeg"),              -- Evt Cuisine 01 : 6 images
            ( "cuisine2.jpeg"),
            ( "cuisine3.jpeg"),
            ( "cuisine4.jpeg"),
            ( "cuisine5.jpeg"),
            ( "cuisine6.jpeg"),

            ( "cuisine2.jpeg"),              -- Evt Cuisine 02
            ( "cuisine3.jpeg"),              -- Evt Cuisine 03
            ( "cuisine4.jpeg"),              -- Evt Cuisine 04
            ( "cuisine5.jpeg"),              -- Evt Cuisine 05
            ( "cuisine6.jpeg"),              -- Evt Cuisine 06
            ( "cuisine8.jpeg"),              -- Evt Cuisine 07

            ( "cours1.jpeg"),          -- Evt Cour 1 a 5
            ( "cours2.jpeg"),
            ( "cours3.jpeg"),  
            ( "cours4.jpeg"),  

            ( "cours2.jpeg"),          -- Evt Cour 1 a 5
            ( "cours3.jpeg"),          -- Evt Cour 1 a 5
            ( "cours4.jpeg"),          -- Evt Cour 1 a 5
            ( "cours5.jpeg"),          -- Evt Cour 1 a 5

            ( "jardin1.jpeg"),          -- Evt Jardin 1 4 images
            ( "jardin2.jpeg"),
            ( "jardin3.jpeg"),
            ( "jardin4.jpeg"),

            ( "jardin2.jpeg"),          -- Evt Jardin 2 3 images
            ( "jardin3.jpeg"),               
            ( "jardin4.jpeg");              


INSERT INTO content (titre, soustitre, texte)
    VALUES 
            ( "Titre1", "SousTitre1", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre2", "SousTitre2", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre3", "SousTitre3", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre4", "SousTitre4", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre5", "SousTitre5", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre6", "SousTitre6", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre7", "SousTitre7", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre8", "SousTitre8", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre9", "SousTitre9", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre10", "SousTitre10", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre11", "SousTitre11", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre12", "SousTitre12", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre13", "SousTitre13", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre14", "SousTitre14", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." ),
            ( "Titre15", "SousTitre15", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, hic numquam velit delectus sunt molestias explicabo
eaque obcaecati inventore exercitationem blanditiis vitae aspernatur voluptas fugiat at soluta in. Voluptate, dolorem!
Tempore nisi eius repudiandae dolore aspernatur nihil illo sapiente molestias qui soluta quod, magni quibusdam in fuga
odit nemo ipsam impedit tempora amet expedita, illum neque perferendis? Odio, quis reiciendis." )
            

