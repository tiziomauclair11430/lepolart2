<?php 

include 'debug.php';

$host = 'localhost';
$db   = 'polart';
$user = 'moi';
$pass = 'test';

// CONNEXION BDD =====================================================

try {
    $pdo = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}


// CRUD : Create ===================================================
// insérer ici les function de création / insertion...


// CRUD : Read =====================================================

function getInfo() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM info;");
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getArtiste() {
    global $pdo;
    $req = $pdo->query('SELECT nom, prenom, url FROM artiste;');
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getActu(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM evenement ORDER BY date_evt DESC LIMIT 5;');
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getMOP() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'mop';");
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

function getMopEvents() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM evenement WHERE ;");
}

function getProd() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'prod';");
    return $req->fetchAll();
}

// function getEvts($cat) {
//     global $pdo;
//     $req = $pdo->query("SELECT * FROM evenement WHERE id_cat = '$cat';");
//     return $req->fetchAll();
// }

function getEvts($idCat) {
    global $pdo;
    $sql = "SELECT * FROM evenement WHERE id_cat = ?";
    $req = $pdo->prepare($sql);
    $req->execute([$idCat]);
    return $req->fetchAll();
}


function getEvtId($id) {
    global $pdo;
    $sql = "SELECT * FROM evenement WHERE id = ?";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetch();
}
function getCatId($id) {
    global $pdo;
    $sql = "SELECT * FROM cat WHERE id = ?";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetch();
}

function getEvtsN1N2($idCat, $num1, $nb ) {
    global $pdo;
    // $sql = "SELECT * FROM evenement WHERE id_cat = '$idCat' ORDER BY 'id' ASC LIMIT $num1, $nb ";
    $sql = "SELECT * FROM evenement WHERE id_cat = :idCat ORDER BY id ASC LIMIT :num1 , :nb";
    $req = $pdo->prepare($sql);
    $req->bindValue(':idCat', $idCat, PDO::PARAM_INT);
    $req->bindValue(':num1', $num1, PDO::PARAM_INT);
    $req->bindValue(':nb', $nb, PDO::PARAM_INT);
    $stm = $req->execute( );
    return $req->fetchAll();
}

function getEvtImages($id) {
    global $pdo;
    $sql = "SELECT fichier, ordre FROM images INNER JOIN evenement_images
            ON images.id = evenement_images.id_image
            WHERE id_evenement = ?
            ORDER BY ordre ASC";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetchAll();
}

function getEvtContents($id) {
    global $pdo;
    $sql = "SELECT soustitre, texte, ordre FROM content INNER JOIN evenement_content
            ON content.id = evenement_content.id_content
            WHERE id_evenement = ?
            ORDER BY ordre ASC";
    $req = $pdo->prepare($sql);
    $req->execute([$id]);
    return $req->fetchAll();
}

// CRUD : Update =====================================================

// update la table info depuis l'admin
function updateInfo($section1, $section2, $section3, $section4, $url) {
    global $pdo;
    $req = $pdo->prepare("UPDATE info SET section1 = ?;")->execute([$section1]);
    $req = $pdo->prepare("UPDATE info SET section2 = ?;")->execute([$section2]);
    $req = $pdo->prepare("UPDATE info SET section3 = ?;")->execute([$section3]);
    $req = $pdo->prepare("UPDATE info SET section4 = ?;")->execute([$section4]);
    $req = $pdo->prepare("UPDATE info SET url_adhesion = ?;")->execute([$url]);
}

function updateCat($array) {
    global $pdo;
    $i = 1;
    foreach ($array as $cat) {
        $req = $pdo->prepare("UPDATE cat SET nom = ? WHERE id = ?;")->execute([$cat["cat"], $i]);
        $req = $pdo->prepare("UPDATE cat SET img = ? WHERE id = ?;")->execute([$cat["url"], $i]);
        $req = $pdo->prepare("UPDATE cat SET soustitre = ? WHERE id = ?;")->execute([$cat["titre"], $i]);
        $req = $pdo->prepare("UPDATE cat SET text_description = ? WHERE id = ?;")->execute([$cat["text"], $i]);
        $i++;
    }
}

function updateDescriptionAsso($text) {
    global $pdo;
    $req  = $pdo->prepare("UPDATE info SET description_asso = ?;")->execute([$text]);
}

function addArtist($nom, $prenom, $url) {
    global $pdo;
    $sql = "INSERT INTO artiste (nom, prenom, url) VALUES (?,?,?);";
    $req = $pdo->prepare($sql)->execute([$nom, $prenom, $url]);
}

function addEvenement($titre, $id_cat,$date, $lieu, $img ,$lien){
    try{
    global $pdo;
    $req = $pdo->prepare("INSERT INTO evenement(nom, id_cat,date_evt, lieu, img_princ, lien) VALUES (?,?,?,?,?,?);");
    $req->execute([$titre, $id_cat,$date, $lieu,$img ,$lien]);
    return $pdo->lastInsertId();
    }catch(Exception $e){
            // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
        
    }
}

function addContent($st, $sst, $paragraphe){
    try{
        global $pdo;
        $req = $pdo->prepare("INSERT INTO content(titre, soustitre, texte) VALUES (?,?,?);");
        $req->execute([$st, $sst, $paragraphe]);
        return $pdo->lastInsertId();
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

function addImages($img){
    try{
        global $pdo;
        $req = $pdo->prepare("INSERT INTO images(fichier) VALUES (?);");
        $req->execute([$img]);
        return $pdo->lastInsertId();
        }catch(Exception $e){
                // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
            
        }
}

// CRUD : Delete =====================================================







// =================================PP session2=================================

function admin2($nom,$cat,$date,$lieu,$img,$lien){
    try{
    global $pdo;
    $req = $pdo->prepare("INSERT INTO evenement(nom, id_cat, date_evt, lieu, img_princ, lien) VALUES ('?,?,?,?,?,?')");
    $req->execute([$nom,$cat,$date,$lieu,$img,$lien]); 
    }catch(Exception $e){
        // en cas d'erreur :
    echo " Erreur ! ".$e->getMessage();
    echo $req;

    }
}


function getMasterRequete()
{

    global $pdo;
    $req = $pdo->query("SELECT * FROM evenement 
    INNER JOIN evenement_images
        ON evenement.id = evenement_images.id_evenement 
    INNER JOIN images 
        ON images.id = evenement_images.id_image 
    INNER JOIN evenement_content 
        on evenement.id = evenement_content.id_evenement
    INNER JOIN content
        On evenement_content.id_content = content.id
    INNER JOIN cat_evenement
        ON evenement.id = cat_evenement.id_evenement
    INNER JOIN cat
        ON cat.id = cat_evenement.id_cat"
        );
    return $req->fetchAll();

}

function getEventById($id){
    return getMasterRequete()[intval($id)];
}

function getMasterRequete2($id)
{

    global $pdo;
    $req = $pdo->prepare("SELECT * FROM evenement 
    INNER JOIN evenement_images
        ON evenement.id = evenement_images.id_evenement 
    INNER JOIN images 
        ON images.id = evenement_images.id_image 
    INNER JOIN evenement_content 
        on evenement.id = evenement_content.id_evenement
    INNER JOIN content
        On evenement_content.id_content = content.id
    INNER JOIN cat_evenement
        ON evenement.id = cat_evenement.id_evenement
    INNER JOIN cat
        ON cat.id = cat_evenement.id_cat WHERE evenement.id = ?"
        );
        $req->execute([$id]);
    return $req->fetchAll();

}

function liaison_Categorie_Evenement($idEvenement, $idCategorie) {
    try{
    global $pdo;
    $req = $pdo->prepare('INSERT INTO cat_evenement(id_evenement,id_cat) VALUES (?,?)');
    $req->execute([$idEvenement, $idCategorie]);
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

function getIdEvenement() {
    global $pdo;
    $req = $pdo->query('SELECT * FROM evenement');
    return $req->fetch();
}

function liaison_Content_Evenement($idEvenement, $idContent, $ordre) {
    try{
    global $pdo;
    $req = $pdo->prepare("INSERT INTO evenement_content(id_evenement,id_content,ordre) VALUES (?,?,?)");
    $req->execute([$idEvenement, $idContent, $ordre]);
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        
    }

}

function liaison_Image_Evenement($idEvenement, $idImage, $ordre) {
    try{
    global $pdo;
    $req = $pdo->prepare("INSERT INTO evenement_images(id_evenement,id_image,ordre) VALUES (?,?,?)");
    $req->execute([$idEvenement, $idImage, $ordre]);
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
    }
}

function delete($id){
    global $pdo;
    try{ 
        $req = $pdo->prepare("DELETE FROM artiste WHERE id = ?");
        $req->execute([$id]);
    
    // header('Location:index.php');
        }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
        }
    
}

function updateCat2(string $titre,string $soustitre,string $texte,string $img,int $id) {
    global $pdo;
    $pdo->prepare("INSERT INTO content(titre, soustitre, texte) VALUES (?,?,?);")->execute([$titre, $soustitre, $texte]);
    $pdo->prepare("UPDATE cat SET nom = ?, img = ?, soustitre = ?, text_description = ? WHERE id = ?;")->execute([$titre, $img, $soustitre, $texte, $id]);
}




// function getId($id){
//     global $pdo;
//     $req = $pdo->query("SELECT id FROM evenement ");
//     return $req->last
// }


?>