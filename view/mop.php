<?php 
include '../model/data.php';
$onglets  = getInfo()[0];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de LEPOL'ART">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick-theme.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <title>Menu Orchestre Participatif</title>
</head>

<body>
    <div class="header">
        <?php 
    include 'header.php'
    ?>
    </div>
    <main>
        <div class="wrapper">
            <div class="container-full">
                <?php $list = getMOP(); ?>
                <?php foreach($list as $element) { ?>
                <div class="container">
                    <div class="box-title">
                        <a href="mop_detail.php?nom=<?php  echo $element["nom"]; ?>&id=<?php  echo $element["id"]; ?>"><img
                                src="./<?php  echo $element["img"]; ?>" alt="Orchestre Participatif" class="img-cote"
                                data-name="<?php  echo $element["nom"]; ?>"></a>
                        <span class="title"><?php  echo $element["nom"]; ?></span>
                    </div>
                    <div class="box-text">
                        <h2 class="sub-title"><?php  echo $element["soustitre"]; ?></h2>
                        <p class="p-header"><?php  echo $element["text_description"]; ?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </main>

    <?php 
    include 'footer.php'
    ?>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="../control/mopapp.js"></script>
</body>

</html>