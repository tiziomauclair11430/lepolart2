<?php include '../../model/data.php'; ?>

<?php
include './secured.php';

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="admin.css">
    <title>Admin</title>
</head>

<body class="col-center mb100">
    <!-- Onglets Nav -->
    <div class="w60">
        <p class="titre-section">onglets nav :</p>
    </div>
    <form class="container-onglets w60" action="../../control/control_nav.php" method="POST">
        <?php $onglets = getInfo()[0]; ?>
        <input type="text" required value="<?php echo $onglets["section1"]; ?>" name="section1">
        <input type="text" required value="<?php echo $onglets["section2"]; ?>" name="section2">
        <input type="text" required value="<?php echo $onglets["section3"]; ?>" name="section3">
        <input type="text" required value="<?php echo $onglets["section4"]; ?>" name="section4">
        <input type="text" required value="<?php echo $onglets["url_adhesion"]; ?>" name="url_adhesion">
        <div class="w60">
            <button type="submit">sauvegarder</button>
        </div>
    </form>

    <!-- Section / categories -->
    <div class="w60">
        <p class="titre-section">Catégories</p>
    </div>
    <form class="container-categorie" action="../../control/control_categorie.php" method="POST" enctype="multipart/form-data">
        <div id="card-container">
            <?php $cats = getMOP();
            //var_dump($onglets);
            $i = 1;
            foreach ($cats as $cat) { ?>
                <div class="categorie-card">
                    <input type="text" required value="<?php echo $cat["nom"]; ?>" name="cat<?php echo $i; ?>">
                    <input type="file" value="<?php echo $cat["img"]; ?>" name="url<?php echo $i; ?>">
                    <p>apercu de l'image</p>
                    <img src="../upload/<?php echo $cat["img"]; ?>">
                    <input type="text" required value="<?php echo $cat["soustitre"]; ?>" name="titre<?php echo $i; ?>">
                    <textarea name="text<?php echo $i; ?>" cols="30" rows="10"><?php echo $cat["text_description"]; ?></textarea>
                </div>

            <?php $i++;
            } ?>
        </div>

        <div class="w60">
            <button type="submit">sauvegarder</button>
        </div>
    </form>

    <div class="w60">
        <p class="titre-section">description de l'association</p>
    </div>
    <form action="../../control/control_description.php" method="post" id="description-form" class="w60">
        <textarea id="description-area" form="description-form" cols="30" rows="10" name="description_asso">
            <?php echo getInfo()[0]["description_asso"]; ?>
        </textarea>
        <div class="w60">
            <button type="submit">sauvegarder</button>
        </div>
    </form>

    <div class="w60">
        <p class="titre-section">artistes intervenants</p>
    </div>
    <form action="../../control/sauv_artiste.php" method="post" id="artist-form">
        <div id="tab-artiste">

            <?php $tabCat = getArtiste()[0]; ?>
            <table>
                <thead>
                    <tr>
            <th class="tab-cat case">nom</th>
            <th class="tab-cat case">id</th>
            <th class="tab-cat case">supprimer</th>
            <tr>
                </thead>
        
<tbody>
            <?php $artisteList = getArtiste();
            foreach($artisteList as $artist) { ?>
          <tr>
           
            <th><?php echo $artist["nom"]; ?></th> 
            <th><?php echo $artist["id"]; ?></th>
            <th><a href="../../control/suppartiste.php? id=<?php echo $artist["id"]; ?>" name="supp">Supprimer</a></th>
            </tr><?php
         
        } ?>
        </tbody>
        
    </table></div>
    </form>
   
    <div class="w60">
        <input type="submit" form="artist-form" value="sauvegarder">
    </div>

    <form action="../../control/ajouter_artiste.php" method="POST">
            <input type="text" required placeholder="nom" name="nom">
            <input type="text" required placeholder="prenom" name="prenom">
            <input type="text" required placeholder="url" name="url_artiste">
            <input type="submit" value="ajouter l'artiste">
    </form>



</body>

</html>