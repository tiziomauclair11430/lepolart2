<?php

session_start();
global $site_domain;
if(!$_SESSION['auth']){
    header('Location: '.$site_domain.'/view/login.php');
}
?>