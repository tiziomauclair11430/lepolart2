<?php 
include "secured.php";
$id_evenement = $_GET['Id'];
$ordre = $_GET['Ordre'];
if(!$ordre && $ordre !==1){
    $ordre = 1;
}else{
    $ordre++;
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Ajouter des images au post</h1>

    <form action="../../control/ajouter_images.php" method="post" enctype="multipart/form-data">
        <input type="file" name="img_supp">
        <input type="hidden" value="<?php echo htmlspecialchars($id_evenement); ?>" name="id_evenement">
        <input type="number" value="" name="ordre">
        <input type="submit" value="Ajouter une nouvelle image">
    </form>

    <div class="image">
        <?php foreach(getEvtImages($id_evenement) as $select){ ?>
        <img src="../view<?php echo $select['fichier']?>">
        <?php } ?>


       
    </div>
</body>
</html>