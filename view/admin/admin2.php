<?php include "../../model/data.php"?>

<!-- <?php
include './secured.php';

?> -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>admin2</title>
    <style>
        .form{
            width: 75%;
            height: auto;
        }
    </style>
</head>
<body>
    <div class="form">
    <form action="../../control/ajouter_evenement.php" method="post" enctype="multipart/form-data">
        <label for="cat" class="form-label">categorie :</label>
        <select name="cat" class="form-select form-select-sm">
            <option value="1">coté jardin</option>
            <option value="2">coté cours</option>
            <option value="3">coté cuisine</option>
            <option value="4">production</option>
        </select>
        <label for="titre" class="form-label">titre :</label>
        <input type="text" name="titre" class="form-control">

        <label for="date" class="form-label">date :</label>
        <input type="date" name="date" class="form-control">

        <label for="lieu" class="form-label">lieu :</label>
        <input type="text" name="lieu" class="form-control">

        <label for="lien_D'inscription" class="form-label">lien d'inscription : </label>
        <input type="text" name="lien_D'inscription" class="form-control">

        <label for="img" class="form-label">image principal :</label>
        <input type="file" name="img" class="form-control">

        <label class="form-label">sous titre</label>
        <input type="text" name="st" class="form-control">

        <label class="form-label">sous-sous titre</label>
        <input type="text" name="sst" class="form-control">

        <label class="form-label">paragraphe</label>
        <textarea name="paragraphe" class="form-control"></textarea>
        
        <button class="btn btn-primary" type="submit">Valider</button>
    </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>