<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de LEPOL'ART">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="./style/style.css">

    <title>Productions</title>
</head>

<body class="col-center">
    <?php  
        include "./../model/data.php";
        $onglets  = getInfo()[0];
        include 'header.php'
    ?>
    <main class="vw80">
        
        <?php
            // On détermine sur quelle page on se trouve
            $currentPage = 0;
            if(isset($_GET['page']) && !empty($_GET['page'])){
                $currentPage = (int) strip_tags($_GET['page']);
            }else{
                $currentPage = 1;
            }

            // On récupère le nb total de productions
            $nbEvts = count( getEvts(4) );

            // On détermine le nombre d'articles par page
            $parPage = 8;

            // On calcule le nombre de pages total
            $nbPages = ceil($nbEvts / $parPage);
            
            // Calcul du 1er article de la page
            $premier = ($currentPage * $parPage) - $parPage;
            // echo "</p> nbEvts = $nbEvts";
            // echo "</p> parPage = $parPage";
            // echo "</p> nbPages = $nbPages";
            // echo "</p> premier = $premier";    
        ?>

        <div id="headbarre"></div>
        <h2 class=" padding-all center-txt">CREATIONS DISPONIBLES MULTI-CONTEXTES</h2>
        <h3 class="center-txt padding-bot20"> → Force d’adaptation et innovation </h3>

        <div class="col">
            <div class="ligne axe1-sp-around padding-V10">
                <!-- foreach 4 img  -->
                <!-- <h3> "LIGNE 1: premier = $premier"; </h3> -->
                <?php $prods = getEvtsN1N2(4, $premier, 4);
                foreach($prods as $prod_i) { ?>
                    <div>
                        <a href="./prod_detail.php?page=<?= $prod_i["id"] ?>" >
                            <img src= <?php echo "./upload/".$prod_i["img_princ"] ?> alt="img production" class="img-cote2">
                        </a>    
                    </div>
                <?php } ?>
            </div>

            <div class="ligne axe1-sp-around padding-V10">
                <!-- foreach 4 img  -->
                <!-- <h3> "LIGNE 2: premier = ", <?php echo $premier+4 ?> </h3> -->
                <?php 
                $premier +=4;
                $prods = getEvtsN1N2(4, $premier, 4);
                foreach($prods as $prod_i) { ?>
                    <div>
                        <a href="./prod_detail.php?page=<?= $prod_i["id"] ?>" >
                            <img src= <?php echo "./upload/".$prod_i["img_princ"] ?> alt="img production" class="img-cote2">
                        </a>   
                    </div>
                <?php } ?>
            </div> <!-- Fin col 2 lignes -->
            
            <nav>
                <ul class="pagination margin-top30 center-elt">
                    <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
                    <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                        <a href="./productions.php?page=<?= $currentPage - 1 ?>" class="page-link"> << </a>
                    </li>
                    <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                    <li class="page-item"> 
                        <div class="page-link"> <?= $currentPage ?> </a>
                    </li>
                    <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                    <li class="page-item <?= ($currentPage == $nbPages) ? "disabled" : "" ?>">
                        <a href="./productions.php?page=<?= $currentPage + 1 ?>" class="page-link"> >> </a>
                    </li>
                </ul>
            </nav>

        </div> <!-- Fin col Ligne1 Ligne2 Nav -->

    </main>

    <?php 
    include 'footer.php'
    ?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>
