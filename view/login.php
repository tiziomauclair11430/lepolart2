<?php
// Demarre une session pour un utilisateur
session_start();

include 'debug.php';
include "./../model/data.php";


/*Lorsque l'utilsateur clique sur le bouton d'envoi, je déclare mes variables */ 
if(isset($_POST['envoi'])){
    global $pdo;
    $name = $_POST['name'];
    $password = $_POST['password'];
// Je recupere sous forme de tableau,le mot de passe et le pseudo dans la table admin de ma base de donnée
    $recup_user = $pdo->prepare('SELECT * FROM administrateur WHERE pseudo = ? AND mdp = ?');
    $recup_user->execute(array($name, $password));
// Je verifie si les informations sont présentes dans la table
    
    if($recup_user->rowCount() > 0){
        global $pdo;
        $_SESSION['pseudo'] = $name;
        $_SESSION['auth'] = true;
        // $_SESSION['id'] = $recup_user->fetch(['id']);
// Si les identifiants sont correct : l'utilisateur est renvoyé vers add.php
        global $site_domain;
        header('Location: '.$site_domain.'/view/admin/admin.php');
    }else{
// Si les identifiants sont incorrect : l'utilisateur est renvoyé vers index.html via la balise meta HTML   
        $bye = "Vos identifiants sont incorrects...";
    }
}
?>
 

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="login.css">
     <title>Login</title>
 </head>
 <body>
 <div id="container">
            <!-- zone de connexion -->
            
            <form action="login.php" method="POST">
                <h1>Connexion</h1>
                
               
                <p><b>Name</b></p>
                <input class="logtext" type="text" name="name" placeholder="Entrer le nom d'utilisateur">
                <p><b>Password</b></p>
                <input type="password" name="password" placeholder="Entrer le mot de passe">
                <input class="log" type="submit" name="envoi" value="Login">

                
            </form>
        </div>
 </body>
 </html>
