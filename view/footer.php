<footer>
    <div class="foottxt">
        <p> <span>Nous Contacter :</span></p>
        <p> <span>➤</span> 1A,avenue d’Argeliers 11590 Ouveillan France</p>
        <p><span>☎</span><a href="tel:+33606631202">+33 (0)6.06.63.12.02</a> / <a href="mailto:lepolart.asso@gmail.com">lepolart.asso@gmail.com</a></p>
        <p><span>© LEPOL’ART Tous Droits Reservés</span></p>
    </div>

    <div class="partIMG"><img src="./assets/LOGOpart.jpg" alt="logoPartenaires"></div>
</footer>