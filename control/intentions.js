let rSize = document.getElementById("imgBan").height;

// myTags -> index.php
const tabInter = [];

for (let elem of myTags) {
  // let ancre = `<a href="elem["url"]">elem["nom"]</a>;`
  // tabInter.push(ancre);
  tabInter.push(elem["nom"]);
}

var tagCloud = TagCloud("#animBanBox", tabInter, {
  // radius in px
  radius: rSize,

  // animation speed
  // slow, normal, fast
  maxSpeed: "fast",
  initSpeed: "normal",

  // 0 = top
  // 90 = left
  // 135 = right-bottom
  direction: 225,

  // interact with cursor move on mouse out
  keep: false,
});

// ________________________________________________________________________________________________________

const eventBulle = document.getElementsByClassName("bulle");

for (let bulle of eventBulle) {
  bulle.addEventListener("mouseover", eventAnimation);
}

function eventAnimation(event) {
  let id = parseInt(event.target.id);
  const eventListe = document.getElementsByClassName("eventDetails");

  for (let elem of eventListe) {
    elem.classList.remove("eventACTIF");
    elem.classList.remove("eventACTIF2");
    elem.classList.add("eventINACTIF");
  }

  let selection = document.getElementById("event" + id);
  selection.classList.remove("eventINACTIF");
  selection.classList.add("eventACTIF");

  let selectionBefore = document.getElementById("event" + (id - 1));
  selectionBefore.classList.remove("eventINACTIF");
  selectionBefore.classList.add("eventACTIF2");

  let selectionAfter = document.getElementById("event" + (id + 1));
  selectionAfter.classList.remove("eventINACTIF");
  selectionAfter.classList.add("eventACTIF2");
}

// AddEvent on Scroll
const box = document.getElementById("eventBox");
window.addEventListener("scroll", function () {
  scrollValue =
    ((window.innerHeight + window.scrollY) / document.body.offsetHeight) * 100;
  // console.log(scrollValue);

  if (scrollValue > 56) {
    box.style.opacity = "1";
    box.style.transform = "none";
  }
});

const btnAsso = document.getElementById("btnAsso");
