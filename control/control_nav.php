<?php 

include '../model/data.php';

if(
    isset($_POST["section1"]) && $_POST["section1"] !== "" &&
    isset($_POST["section2"]) && $_POST["section2"] !== "" &&
    isset($_POST["section3"]) && $_POST["section3"] !== "" &&
    isset($_POST["section4"]) && $_POST["section4"] !== "" &&
    isset($_POST["url_adhesion"]) && $_POST["url_adhesion"] !== "" 
) {
    $section1 = $_POST["section1"];
    $section2 = $_POST["section2"];
    $section3 = $_POST["section3"];
    $section4 = $_POST["section4"];
    $url = $_POST["url_adhesion"];

    updateInfo($section1, $section2, $section3, $section4, $url);
} 
header('Location: ../view/admin/admin.php');

?>