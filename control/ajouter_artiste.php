<?php 

include '../model/data.php';

if(
    isset($_POST["nom"]) && $_POST["nom"] !== "" &&
    isset($_POST["prenom"]) && $_POST["prenom"] !== "" &&
    isset($_POST["url_artiste"]) && $_POST["url_artiste"] !== "" 
) {

$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$url = $_POST["url_artiste"];

addArtist($nom, $prenom, $url);
}

header('Location: ../view/admin/admin.php'); 
?>

